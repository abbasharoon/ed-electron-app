
# Electron Browser App

## Requirements
#### NodeJs
Download and install the latest version of NodeJs for your OS from [here](https://nodejs.org/en/download/)
#### Google Chrome 
Make sure Google Chrome is installed in the default location. Google chrome can be downloaded from [here](https://www.google.com/chrome/)
To check the exact path to Google Chrome executable, go to [chrome://version](chrome://version)
The paths to chrome executable should correspond to the following depending on the OS:

 - **Win**: [C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe](C:%5CProgram%20Files%20%28x86%29%5CGoogle%5CChrome%5CApplication%5Cchrome.exe)
 - **Mac**: [/Applications/Google Chrome.app/](/Applications/Google%20Chrome.app/)

## Project Installation
1. First clone the project from online repository
`git clone https://abbasharoon@bitbucket.org/abbasharoon/ed-electron-app.git`
2. Then go to the app directory `cd app`
3. Install the project by running `npm `install`

## Developmental Build
Electron helps launch the project without requiring building it on each change to test in developmental environment. Since we are using [electron-forge](https://www.electronforge.io/) in order to make the development faster so the developmental build can be easily launched by running:
`electron-forge start` in the app directory

Please note that you will need to run that command everything you make some changes as nodejs loads all the scripts at launch time and changes aren't synced in real time to the running process. 


## Production Build

For production build, simply run `electron-forge make` and it will save binaries to `app/out/make` for the desired OS. By the default, it compiles for the platform on which compilation is done 


## Configurations
Configurations are stored in `app/config.js` as as JavaScript Object. Keys for each value are mostly self explanatory and can be updated by just replacing the values in the file 
