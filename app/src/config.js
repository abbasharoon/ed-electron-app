const config = {
  gmail: {
    email: 'carenetla1@gmail.com',
    password: '(Caregivers4u)'
  },
  paraphrase: '(Caregivers4u)',
  paths: {
    win: 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
    mac: '/Applications/Google Chrome.app/'
  },
  dimension: {
    mainWidth: 0.83,
    modalWidth: 0.17
  },
  links: {
    gmail: 'https://accounts.google.com/signin/chrome/sync?ssp=1&continue=https%3A%2F%2Fwww.google.com%2F',
    chromeSync: 'chrome://settings/syncSetup',
    modalWindow: 'https://connectstreams.firebaseapp.com/',
    firstPage: 'https://d-9267162c05.awsapps.com/start'
  },
  selectors: {
    gmail: {
      email: '#identifierId',
      password: '#password input',
      nextButton: '#passwordNext',
    },
    paraphrase: `document.querySelector('settings-ui').shadowRoot
             .children['container']
             .children[2]
             .shadowRoot
             .children[10]
             .shadowRoot
             .children[8]
             .children[2]
             .children[0]
             .shadowRoot
             .children[8]
             .children[1]
             .children[0]
             .shadowRoot
             .children[11].children[1].children[0].shadowRoot.children[6].children[0].children[0].children.input`,
    paraphraseButton: `document.querySelector('settings-ui').shadowRoot
             .children['container']
             .children[2]
             .shadowRoot
             .children[10]
             .shadowRoot
             .children[8]
             .children[2]
             .children[0]
             .shadowRoot
             .children[8]
             .children[1]
             .children[0]
             .shadowRoot
             .children[11].children[1].children[0].children[0]`,
    syncConfirmButton: `document.querySelector("body > settings-ui").shadowRoot.querySelector("#main").shadowRoot.querySelector("settings-basic-page").shadowRoot.querySelector("#basicPage > settings-section.expanded > settings-people-page").shadowRoot.querySelector("#pages > settings-subpage > settings-sync-page").shadowRoot.querySelector("settings-sync-account-control").shadowRoot.querySelector("#setup-buttons > cr-button.action-button")`,
    syncOnButton: `document.querySelector("body > settings-ui").shadowRoot.querySelector("#main").shadowRoot.querySelector("settings-basic-page").shadowRoot.querySelector("#basicPage > settings-section.expanded > settings-people-page").shadowRoot.querySelector("#pages > settings-subpage > settings-sync-page").shadowRoot.querySelector("settings-sync-account-control").shadowRoot.querySelector("#sign-in")`,
    confirmPageOnSync: `document.querySelector("body > sync-confirmation-app").shadowRoot.querySelector("#confirmButton")`,
  },
  jabraImage: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAGoUlEQVRogdWZe2xTVRzHf/e5thRbXm1huheMjY4xaDuEbRQn9Q9Cwljj3YhEhGAwaGLcJCQYNBcj/mNC4iIgW4wBlD8Q4mRbJ0hwItvc1i2EkeEiIR2TUYTSxy23vevj+oe9s2PSrq9VPkmTc07P+f2+39x7Hj0FeMZBkh1wx44dokWLFi25fPmybGxsDMRiMeh0OmAY5rpGo/HQNO1PZr6EDdA0jXZ1da2y2Wyve71ew/j4eCHHcdiURAgCIpHokUgk6uR5/mx5eXnLsWPH7Inmj9sATdNoc3PzBpZlP+U4TsfzfEzjMQxzKxSKkyUlJZ80Njbei1dHXAa2bt26aHh4+ITNZjNEDI4gEM0YQRD2zMzM965cuXIyHi0xG9Dr9atsNtslhmHmhrfjOP549uzZv+I4fik3N/eWXC6/3tPT43A6nWA0GgvNZnM+wzA6lmUrfT7f8mAw+K8IBAG5XP5FdXX1+zRNj6fMgEajWWG32zsCgcAcoQ3DMOu8efMOL1iwoKG9vZ2LFoOmaXRgYGCtxWI56HQ6N4Q/IYVC0bZv3z5jTU1NTCamhdFofH7p0qW2rKwsXvgUFBR8v3v37vnxxqyoqKDy8vIc4TG1Wm1DMnUDAMDx48eJ4uLizvBEhYWFh5MRu7a2tlStVruEuNnZ2XxFRUV1MmJPUFpauiNc/IoVK5qTGd9oNK7Jzs5mhPj5+flWmqbnRh8JMGW9fhKKosQjIyNtfr9/FgAAhmG3OY572eFwJG1Dunnz5p9FRUV/ulyuagCAQCAgHR0dxe7du/dTtLFotA4jIyPbvV6vQqgvW7bsQ4vF4k1M8lS6urpOyeXyn4W6y+V6e//+/fOijYtqwOVyvSOUpVLpgMlkOh2/zMiQJLkHRf+RND4+LjGZTG9EGxPRgF6vz/d4PMVCXSaTfZ6wygiYzeZhuVzeIdQZhtkWbUxEAxzHvSqs0wRBsBRFfZewyiiQJHlUKAeDQc2WLVtyIvWPaODx48dlYYF/qa+v9yQuMTI6na4dwzAPAEAwGASr1bo5Uv+nGti2bdtzHo9HK9SlUml/8mQ+naNHj7pJkjQLdZZlN9TV1Ymf1n+Kgbq6OvHq1asP9fX1/eXz+RYK7QiCDCRf7n+DIEivUHY4HJvb2toser3+rTNnzkxZ9icZ2LVrV3Zra+tVq9X6gdfrzQj/7v79+87USZ6Mx+OZdLz2er0Ki8Xy5cGDB79taGh4Lvy7CQMGg0HW2dl5geM4zUwJjRWGYWqbmppO0DQ9oXui4HQ6m1iWLUiPtOnjdDq3dHR01At1FABg06ZNyx8+fEilT1ZsWK3WAxRFYQAhA6Ojo68FAoH0qooBlmVld+/erQYIGfD7/S+lV1LssCxbARAywLKsMr1yYsfhcCwACBkgSTK9auJAJBIBQMiAVCq9lVY1cSCXyy0AIQM4jrekV05sIAgCmZmZ5wFCBlQq1QmSJGdsp00UsVjcde7cuR6AkIHz588zSqVyL4Ik/ao06aAoyuXl5U3eyAAArl69+tX8+fOb0iNreqAoGly4cOG7JpOpZ6JNKCAIwvf397+lUqkOoCia9N+8iUKSpEOpVBq7u7sbw9ufPE7zvb29hyorK8tlMtkZDMOYGdQ4AUEQEyfhjIyMBwqF4ojBYFje09Pzw5N9I770NE2T3d3dep7ni+12+9m+vr7RVAgOh6Io7Nq1a79zHLdEIpGMqVSq7I6OjqT+p5BStFptvXDBVV5evjda/6jXKjNJVVXVC3a7/WMAAIlEcruoqCjqLUjUm7kZBCEIotnj8SxFURTmzJlDtbS0PDsnhNLS0o+EV2flypWnpjvuf/EKVVVV7Xzw4AENAEAQxHBZWdmedGuaNhs3bty+ePHiYFZWFp+Tk+MyGAyrYhmf0jmwfv36VxAEqc3JySlbt26dZWhoKHxfQbRabf2dO3eOBAIBFMfxgFqt3nnhwoVLseRI2eFn7dq1n42Nje0VriYzMjJcubm55RcvXryh0+kK/H5/46NHj/QAADiOe9Vq9Zutra3fxponJQYoiiro7e0d4nl+0hyTSCS3UBS1u91uDYSePkmSo0qlsqazs/O3eHKlZBIPDg4WPSkeAIBl2SVut7sUADAMw3iZTPZNTU3Ni/GKBwDAE1L6FNRqtd1sNk9pJwgCcBz/A8fxiyUlJV+fPn26f3BwMKFcKZsDarX6stvtrgxr8kul0s1DQ0PtycyTslVozZo13yEIIvP5fKJZs2bdEovFe27cuPFjqvI9s/wNGbB5ldrj6ucAAAAASUVORK5CYII=`,
};

module.exports = config;