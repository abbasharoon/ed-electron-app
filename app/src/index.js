const {app} = require('electron');
const electron = require('electron');
const puppeteer = require('puppeteer-extra');
const config = require('./config');
const fs = require("fs");
const log = require('electron-log');
// Stealth plugin to hide any hints about automation
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());


// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let loginStatus = false;
let loginProcess = false;
let loginUrl = '';
let loginTimer = '';
let browser = '';
// let skipLogin = fs.existsSync('gmailLoginStatus');
let skipLogin = true;
const createWindow = async () => {
  log.info('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
  log.info('++++++++++++++++++++++++++++++++++++++++++++++STARTING APP ++++++++++++++++++++++++++++++++++++++++++++++++++++++');
  log.info('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
  // Get the screen class in order to get screen dimensions
  const screen = electron.screen;
  // Get screen dimensions
  const dimensions = screen.getPrimaryDisplay().size;
  // Launch chrome browser via puppeteer extra
  browser = await puppeteer.launch({
    // Headless should be false else browser will run in background
    headless: false,
    // The path to installed chrome executable based on platform
    executablePath: process.platform === 'win32' ? config.paths.win : config.paths.mac,
    // Disable all of the puppeeter flags to provide safe & normal browser experience
    ignoreDefaultArgs: [
      '--enable-automation',
      '--disable-background-networking',
      '--disable-background-timer-throttling',
      '--disable-client-side-phishing-detection',
      '--disable-default-apps',
      '--disable-dev-shm-usage',
      '--disable-extensions',
      '--disable-hang-monitor',
      '--disable-popup-blocking',
      '--disable-prompt-on-repost',
      '--disable-sync',
      '--disable-translate',
      '--metrics-recording-only',
      '--no-first-run',
      '--safebrowsing-disable-auto-update'
    ],
    // Set browser width to 85% of viewport
    width: (+dimensions.width * config.dimension.mainWidth).toFixed(0),
    // Set height ot the height of screen
    height: dimensions.height,
    // Chrome specific arguments to enforce window position i.e top left and window size i.e 85% wide
    // The user data argument specifies the location of the  user data cache stored by native chrome
    args: [
      '--disable-infobars',
      '--window-position=0,0',
      `--load-extension=${require('path').join(__dirname, 'okpeabepajdgiepelmhkfhkjlhhmofma/2.1.0.1_0')}`,
      `--window-size=${(+dimensions.width * config.dimension.mainWidth).toFixed(0)},${dimensions.height}`,
      `--user-data-dir=${require('path').join(__dirname, 'Electron/Chrome/User Data')}`
    ]
  });

  browser.on('disconnected', () => {
    process.exit(1);
  });
  const target1 = await browser.on('targetcreated', async (target) => {
    if (target.url().indexOf(config.links.modalWindow) > -1) {
      log.info(`Url Opened: ${target.url()}`);
      const p = await target.page();
      const session = await target.createCDPSession();
      await session.send('Emulation.setPageScaleFactor', {
        width: +(dimensions.width * +config.dimension.modalWidth).toFixed(),
        height: dimensions.height,
        pageScaleFactor: 0.8, // 80%
      });
      // await target1.disconnect();
    }
  });

  // Get all thr browser pages
  let pages = await browser.pages();
  // And close the original one. Original one has viewport stuck in it and is not changing
  // Then open a new one
  const firstPage = pages[0];
  firstPage.goto(config.links.firstPage);
  // return;

  let visitedSync = false;
  // await openModal(firstPage, dimensions);
  if (!skipLogin) {
    log.info('Starting the sync process');
    const settingsPage = await browser.newPage();
    // Open the sync settings in chrome
    await settingsPage.goto(config.links.chromeSync);
    const tar = await browser.on('targetchanged', async (target) => {
      console.log(target.url());
      if (target.url().indexOf('https://accounts.google.com/signin/chrome/sync/') > -1) {
        log.info('Sign in Page Opened, running sign in script');
        const page = await target.page();
        loginProcess = false;
        await login(page);
      }
      if (target.url().indexOf('https://accounts.google.com/signin/chrome/sync/deniedsigninrejected') > -1 ||
        target.url().indexOf('https://accounts.google.com/info/unknownerror') > -1) {
        log.info('Sign in seems to failed from google\'s end. Attempting again');
        const page = await target.page();
        loginProcess = false;
        loginTimer ? clearTimeout(loginTimer) : '';
        page.goto(loginUrl);
        // await login(page);
      }
      if (target.url().indexOf('chrome://sync-confirmation/') > -1 && !visitedSync) {
        // if (!loginProcess || !loginStatus) return;

        // visitedSync = true;
        // await page.goto('chrome://sync-confirmation/');
        // const buttonHandle = await page.evaluateHandle(config.selectors.confirmPageOnSync);
        // await buttonHandle.click();
        // await page.close();
        log.info('Sign in successfull. Configuring rest of sync process');

        loginStatus = true;
        // const page = await target.page();
        await setTimeout(async () => {
          log.info('Adding pharaphrase');
          await settingsPage.bringToFront();
          await settingsPage.keyboard.press('Enter');
          const passwordHandle = await settingsPage.evaluateHandle(config.selectors.paraphrase);
          await passwordHandle.click();
          await settingsPage.keyboard.type(config.paraphrase);
          const buttonHandle = await settingsPage.evaluateHandle(config.selectors.paraphraseButton);
          await buttonHandle.click();
          const confirmHandle = await settingsPage.evaluateHandle(config.selectors.syncConfirmButton);
          await confirmHandle.click();

          fs.closeSync(fs.openSync('gmailLoginStatus', 'w'));
          log.info('Sync process completed.');

          const tester =  await browser.newPage();
          // log.info('Go to chrome extension page and wait for Jabra plugin to load')
          await tester.goto('chrome://extensions');
          await tester.bringToFront();
          const pluginStatus = await tester.evaluateHandle(async () => {
            return new Promise((resolve) => {
              setInterval(() => {
                if (document.querySelector("body > extensions-manager").shadowRoot.querySelector("#items-list").shadowRoot.querySelector("#okpeabepajdgiepelmhkfhkjlhhmofma")) {
                  return resolve(document.querySelector("body > extensions-manager").shadowRoot.querySelector("#items-list").shadowRoot.querySelector("#okpeabepajdgiepelmhkfhkjlhhmofma"));
                }
              }, 1000);
            });
          }, {
            timeout: 0,
          });
          if (pluginStatus) {
            log.info('Opening right side window now');
            // await openModal(firstPage, dimensions);
            // tar.disconnect();
          }
        }, 3000);
      }
      return target;
    });
    // console.log(await page.waitForNavigation());
    await setTimeout(async () => {
    }, 2000);
    await setTimeout(async () => {
      log.info('Click the sync button');
      try {
        const buttonHandle = await settingsPage.evaluateHandle(config.selectors.syncOnButton);
        return await buttonHandle.click();
      } catch(e){
        console.log(e);
        log.error('An error occured here');
      }
    }, 2000);
  } else {
    log.info('Skipping login, opening right side window');
    // await firstPage.waitForNavigation({
    //   waitUntil: 'networkidle0'
    // });
    await openModal(firstPage, dimensions);
  }
};

async function login(page) {
  log.info('Starting login script');
  loginUrl = page.url();
  loginTimer = setTimeout(() => {
    if (!loginStatus && !loginProcess) {
      clearTimeout(loginTimer);
      log.info('Login process not completed in one minute. Starting again');
      page.goto(loginUrl);
    }
  }, 60000);
  try {
    await page.waitForSelector(config.selectors.gmail.email, {
      timeout: 60000,
    });// Then click it and type the email address in a normal user manner
    await page.click(config.selectors.gmail.email);
    await page.keyboard.type(config.gmail.email);
    // Press enter to make it appear more like a user
    await page.keyboard.press('Enter');
    //Then wait for the password input to appear
    await page.waitForSelector(config.selectors.gmail.password, {
      visible: true,
      timeout: 60000,
    }).catch((e) => {
      loginProcess= false;
      log.error(e);
      // console.warn(e);
    });//
    // Password input takes sometime to become clickable so we wait for some time
    // await page.waitForNavigation();
    await setTimeout(async () => {
      // Click the field
      await page.click(config.selectors.gmail.password);
      // Type the password
      await page.keyboard.type(config.gmail.password);
      // Then click the next button
      await page.click(config.selectors.gmail.nextButton);
      // Wait till the page gets redirected successfully
      // await page.waitForNavigation();
      // Then close it
      // await settingsPage.close();
      // Refresh the settings page
      // enable request interception
// add header for the navigation requests
      loginProcess = true;
      // await page.waitForResponse(() => {
      // });
      log.info('Sign in process completed. Waiting for navigation to trigger success status');
      await page.waitForNavigation();
      // return await setTimeout(async () => {
      //   if (!loginStatus) {
      //     console.log('starting login');
      //     loginProcess = false;
      //     await page.goto(loginUrl);
      //   } else {
      //     clearTImeout(loginTimer);
      //   }
      //   return true
      // }, 60000);
    }, 3000);
  } catch (e) {
    if (!loginStatus && !loginProcess) {
      loginTimer ? clearTimeout(loginTimer) : '';
      loginProcess = false;
      log.error(e);
      log.info('An error happened, attempting to login again');
      await page.goto(loginUrl);
    }
  }
}

async function openModal(pagez, dimensions) {
  log.info('Running script to open the modal');
  const page = await browser.newPage();
  await page.evaluate(async (data) => {
    const dimensions = data.dimensions;
    const config = data.config;
    console.log('script called');
    // First open a modal window with the defined link and dimensions specified
    const supportWindow = await window.open(config.links.modalWindow, "window name", `width=${(dimensions.width * config.dimension.modalWidth).toFixed()},height:${dimensions.height},menubar=0,toolbar=0,location=0,personalbar=0,status=0,minimizable=0,resizable=0,titlebar=0`);
    // Enforce the dimension to 15% of screen width and 100% height
    supportWindow.resizeTo((dimensions.width * +config.dimension.modalWidth).toFixed(), dimensions.height);
    // Then move the window to the top right
    supportWindow.moveTo((dimensions.width * 0.835).toFixed(), 0);
  }, {
    dimensions: {
      width: dimensions.width,
      height: dimensions.height
    }, config
  });
  log.info('Modal opened');
  page.close();

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    // createWindow();
  }
});